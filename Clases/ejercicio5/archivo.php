<?php  
//declaracion de clase token
	class archivo{
		//declaracion de atributos
		private $nombre;
		private $token;
        private $permitidos= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token= substr(str_shuffle($this->permitidos), 0, 4);
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='El token ha sido destruido: '.$this->token;
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new archivo($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>